Source: libgdal-grass
Maintainer: Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
Uploaders: Francesco Paolo Lovergine <frankie@debian.org>,
           Bas Couwenberg <sebastic@debian.org>
Section: science
Priority: optional
Build-Depends: cmake,
               debhelper-compat (= 13),
               grass-dev (>= 8.4.1),
               libgdal-dev (>= 3.5.0),
               libpq-dev
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-gis-team/gdal-grass
Vcs-Git: https://salsa.debian.org/debian-gis-team/gdal-grass.git
Homepage: https://github.com/OSGeo/gdal-grass
Rules-Requires-Root: no

Package: libgdal-grass
Architecture: any
Section: libs
Depends: ${grass:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: gdal-bin
Description: GRASS extension for the GDAL library
 GDAL is a translator library for raster geospatial data formats.
 As a library, it presents a single abstract data model to the
 calling application for all supported formats. This extension
 provides access to GRASS data via GDAL.
 .
 This package provides the GDAL GRASS plugin.
